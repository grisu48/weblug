package main

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

var cf Config

type Config struct {
	Settings ConfigSettings `yaml:"settings"`
	Hooks    []Hook         `yaml:"hooks"`
}

type ConfigSettings struct {
	BindAddress    string      `yaml:"bind"`          // Bind address for the webserver
	UID            int         `yaml:"uid"`           // Custom user ID or 0, if not being used
	GID            int         `yaml:"gid"`           // Custom group ID or 0, if not being used
	ReadTimeout    int         `yaml:"readtimeout"`   // Timeout for reading the whole request
	WriteTimeout   int         `yaml:"writetimeout"`  // Timeout for writing the whole response
	MaxHeaderBytes int         `yaml:"maxheadersize"` // Maximum size of the receive header
	MaxBodySize    int64       `yaml:"maxbodysize"`   // Maximum size of the receive body
	TLS            TLSSettings `yaml:"tls"`
}

type TLSSettings struct {
	Enabled    bool          `yaml:"enabled"`
	MinVersion string        `yaml:"minversion"`
	MaxVersion string        `yaml:"maxversion"`
	Keypairs   []TLSKeypairs `yaml:"keypairs"`
}

type TLSKeypairs struct {
	Keyfile     string `yaml:"keyfile"`
	Certificate string `yaml:"certificate"`
}

func (cf *Config) SetDefaults() {
	cf.Settings.BindAddress = ":2088"
	cf.Settings.UID = 0
	cf.Settings.GID = 0
	cf.Settings.ReadTimeout = 0
	cf.Settings.WriteTimeout = 0
	cf.Settings.MaxHeaderBytes = 0
}

// Check performs sanity checks on the config
func (cf *Config) Check() error {
	if cf.Settings.BindAddress == "" {
		return fmt.Errorf("no bind address configured")
	}
	for _, hook := range cf.Hooks {
		if hook.Name == "" {
			return fmt.Errorf("hook without name")
		}
		if hook.Route == "" {
			return fmt.Errorf("hook %s with no route", hook.Name)
		}
		if hook.Command == "" {
			return fmt.Errorf("hook %s with no command", hook.Name)
		}
		if hook.Concurrency < 1 {
			hook.Concurrency = 1
		}
		if hook.Command != "" {
			if len(hook.Commands) > 0 {
				return fmt.Errorf("hook %s cannot have command and commands at the same time", hook.Name)
			}
			hook.Commands = append(hook.Commands, hook.Command)
			hook.Command = ""
		}
	}

	return nil
}

func (cf *Config) LoadYAML(filename string) error {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(content, cf); err != nil {
		return err
	}
	return cf.Check()
}

func ParseTLSVersion(version string) (uint16, error) {
	if version == "" {
		return tls.VersionTLS12, nil
	} else if version == "1.3" {
		return tls.VersionTLS13, nil
	} else if version == "1.2" {
		return tls.VersionTLS12, nil
	} else if version == "1.1" {
		return tls.VersionTLS11, nil
	} else if version == "1.0" {
		return tls.VersionTLS10, nil
	} else {
		return 0, fmt.Errorf("invalid tls version string")
	}
}
