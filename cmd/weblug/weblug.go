/*
 * weblug main program
 */
package main

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

type Handler func(http.ResponseWriter, *http.Request)

func usage() {
	fmt.Println("weblug is a webhook receiver")
	fmt.Printf("Usage: %s [OPTIONS] YAML1[,YAML2...]\n\n", os.Args[0])
	fmt.Println("OPTIONS")
	fmt.Println("  -h, --help                Print this help message")
	fmt.Println()
	fmt.Println("The program loads the given yaml files for webhook definitions")
}

// awaits SIGINT or SIGTERM
func awaitTerminationSignal() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		fmt.Println(sig)
		os.Exit(1)
	}()
}

// Perform sanity check on hooks
func sanityCheckHooks(hooks []Hook) error {
	// Check UID and GID settings. When hooks have their own UID and GID settings, we need the main program to run as root (required for setgid/setuid)
	uid := cf.Settings.UID
	if uid == 0 {
		uid = os.Getuid()
	}
	for _, hook := range hooks {
		// If a hook sets a custom uid or gid, ensure we're running as root, otherwise print a warning
		if hook.UID != 0 && uid != 0 {
			fmt.Fprintf(os.Stderr, "Warning: Hook '%s' sets 'uid = %d' but we're not running as root\n", hook.Name, hook.UID)
		}
		if hook.GID != 0 && uid != 0 {
			fmt.Fprintf(os.Stderr, "Warning: Hook '%s' sets 'gid = %d' but we're not running as root\n", hook.Name, hook.GID)
		}
	}
	return nil
}

// Extract the hostname from an URL
func hostname(url string) string {
	hostname := url
	i := strings.Index(hostname, "://")
	if i > 0 {
		hostname = hostname[i+3:]
	}
	i = strings.Index(hostname, ":")
	if i > 0 {
		hostname = hostname[:i]
	}
	return hostname
}

func main() {
	cf.SetDefaults()
	if len(os.Args) < 2 {
		usage()
	}
	for _, arg := range os.Args[1:] {
		if arg == "" {
			continue
		} else if arg == "-h" || arg == "--help" {
			usage()
			os.Exit(0)
		} else {
			if err := cf.LoadYAML((arg)); err != nil {
				fmt.Fprintf(os.Stderr, "yaml error: %s\n", err)
				os.Exit(1)
			}
		}
	}

	if len(cf.Hooks) == 0 {
		fmt.Fprintf(os.Stderr, "error: no webhooks defined\n")
		os.Exit(2)
	}

	// Sanity check
	if err := sanityCheckHooks(cf.Hooks); err != nil {
		fmt.Fprintf(os.Stderr, "hook sanity check failed: %s\n", err)
		os.Exit(3)
	}

	// Drop privileges?
	if cf.Settings.GID != 0 {
		if err := syscall.Setgid(cf.Settings.GID); err != nil {
			fmt.Fprintf(os.Stderr, "setgid failed: %s\n", err)
			os.Exit(1)
		}
	}
	if cf.Settings.UID != 0 {
		if err := syscall.Setuid(cf.Settings.UID); err != nil {
			fmt.Fprintf(os.Stderr, "setuid failed: %s\n", err)
			os.Exit(1)
		}
	}

	server := CreateWebserver(cf)
	mux := http.NewServeMux()
	server.Handler = mux
	if err := RegisterHandlers(cf, mux); err != nil {
		fmt.Fprintf(os.Stderr, "error: %s\n", err)
		os.Exit(1)
	}
	for i, hook := range cf.Hooks {
		log.Printf("Webhook %d: '%s' [%s] \"%s\"\n", i, hook.Name, hook.Route, hook.Command)
	}

	awaitTerminationSignal()

	var listener net.Listener
	var err error
	if cf.Settings.TLS.Enabled {
		log.Printf("Launching tls webserver on %s", cf.Settings.BindAddress)
		listener, err = CreateTLSListener(cf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %s\n", err)
			os.Exit(1)
		}
	} else {
		log.Printf("Launching webserver on %s", cf.Settings.BindAddress)
		listener, err = CreateListener(cf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "error: %s\n", err)
			os.Exit(1)
		}
	}
	server.Serve(listener)

	// read guard, should never ever ever be called.
	// If we end up here, the only safe thing we can do is terminate the program
	panic("unexpected end of main loop")
}

func CreateListener(cf Config) (net.Listener, error) {
	return net.Listen("tcp", cf.Settings.BindAddress)
}

func CreateTLSListener(cf Config) (net.Listener, error) {
	var err error
	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
	}
	if cf.Settings.TLS.MinVersion != "" {
		tlsConfig.MinVersion, err = ParseTLSVersion(cf.Settings.TLS.MinVersion)
		if err != nil {
			return nil, fmt.Errorf("invalid tls min version")
		}
	}
	if cf.Settings.TLS.MaxVersion != "" {
		tlsConfig.MaxVersion, err = ParseTLSVersion(cf.Settings.TLS.MaxVersion)
		if err != nil {
			return nil, fmt.Errorf("invalid tls max version")
		}
	}
	if tlsConfig.MinVersion == tls.VersionTLS10 || tlsConfig.MinVersion == tls.VersionTLS11 {
		fmt.Fprintf(os.Stderr, "warning: using of a deprecated TLS version (< 1.2) is not recommended\n")
	}

	// Create self-signed certificate, when no keyfile and no certificates are present
	if len(cf.Settings.TLS.Keypairs) == 0 {
		return nil, fmt.Errorf("no keypairs provided")
	} else {
		// Load key/certificates keypairs
		tlsConfig.Certificates = make([]tls.Certificate, len(cf.Settings.TLS.Keypairs))
		for i, keypair := range cf.Settings.TLS.Keypairs {
			tlsConfig.Certificates[i], err = tls.LoadX509KeyPair(keypair.Certificate, keypair.Keyfile)
			if err != nil {
				return nil, fmt.Errorf("invalid tls keypair '%s,%s' - %s", keypair.Certificate, keypair.Keyfile, err)
			}
		}

		if len(tlsConfig.Certificates) == 1 {
			log.Printf("Loaded 1 tls certificate")
		} else {
			log.Printf("Loaded %d tls certificates", len(tlsConfig.Certificates))
		}
	}
	return tls.Listen("tcp", cf.Settings.BindAddress, tlsConfig)
}

func CreateWebserver(cf Config) *http.Server {
	server := &http.Server{
		Addr:           cf.Settings.BindAddress,
		ReadTimeout:    time.Duration(cf.Settings.ReadTimeout) * time.Second,
		WriteTimeout:   time.Duration(cf.Settings.WriteTimeout) * time.Second,
		MaxHeaderBytes: cf.Settings.MaxHeaderBytes,
	}
	return server
}

func RegisterHandlers(cf Config, mux *http.ServeMux) error {
	// Register default paths
	mux.HandleFunc("/", createDefaultHandler())
	mux.HandleFunc("/health", createHealthHandler())
	mux.HandleFunc("/health.json", createHealthHandler())
	mux.HandleFunc("/robots.txt", createRobotsHandler())

	// Register hooks
	for _, hook := range cf.Hooks {
		if hook.Route == "" {
			return fmt.Errorf("no route defined in hook %s", hook.Name)
		}
		if hook.Concurrency < 1 {
			hook.Concurrency = 1
		}
		// allow hooks to have individual maxBodySize arguments.
		if cf.Settings.MaxBodySize > 0 && hook.maxBodySize == 0 {
			hook.maxBodySize = cf.Settings.MaxBodySize
		}
		mux.HandleFunc(hook.Route, createHandler(hook))
	}
	return nil
}

// create a http handler function from the given hook
func createHandler(hook Hook) Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Printf("GET %s %s", r.RemoteAddr, hook.Name)

		// Check if this hook is remote-host limited
		if len(hook.Hosts) > 0 {
			allowed := false
			// Extract the queried hostname
			queriedHost := hostname(r.Host)
			for _, host := range hook.Hosts {
				if host == queriedHost {
					allowed = true
					break
				}
			}

			if !allowed {
				// Hook doesn't exist for this host.
				respondNotFound(w, r)
				return
			}
		}

		// Check if adresses are allowed or blocked
		allowed, err := hook.IsAddressAllowed(r.RemoteAddr)
		if err != nil {
			log.Printf("ERR: Error checking for address permissions for hook \"%s\": %s", hook.Name, err)
			w.WriteHeader(500)
			fmt.Fprintf(w, "{\"status\":\"fail\",\"reason\":\"server error\"}")
			return
		}
		if !allowed {
			log.Printf("Blocked: '%s' for not allowed remote end %s", hook.Name, r.RemoteAddr)
			// Return a 403 - Forbidden
			w.WriteHeader(403)
			fmt.Fprintf(w, "{\"status\":\"blocked\",\"reason\":\"address not allowed\"}")
			return
		}

		// Check for basic auth, if enabled
		if hook.HttpBasicAuth.Password != "" {
			username, password, ok := r.BasicAuth()
			if !ok {
				// Return a 403 - Forbidden
				w.WriteHeader(401)
				fmt.Fprintf(w, "{\"status\":\"unauthorized\",\"message\":\"webhook requires authentication\"}")
				return
			}

			if hook.HttpBasicAuth.Password != password || (hook.HttpBasicAuth.Username != "" && hook.HttpBasicAuth.Username != username) {
				// Return a 403 - Forbidden
				w.WriteHeader(403)
				fmt.Fprintf(w, "{\"status\":\"blocked\",\"reason\":\"not authenticated\"}")
				return
			}

		}

		// Check for available slots
		if !hook.TryLock() {
			log.Printf("ERR: \"%s\" max concurrency reached", hook.Name)
			// Return a 503 - Service Unavailable error
			w.Header().Add("Content-Type", "application/json")
			w.Header().Add("Retry-After", "120") // Suggest to retry after 2 minutes
			w.WriteHeader(503)
			fmt.Fprintf(w, "{\"status\":\"fail\",\"reason\":\"max concurrency reached\"}")
			return
		}

		// Input buffer used to pass the headers to the process
		var buffer bytes.Buffer
		for k, v := range r.Header {
			buffer.WriteString(fmt.Sprintf("%s:%s\n", k, strings.Join(v, ",")))
		}
		buffer.WriteString("\n")
		// Receive body only if configured. By default the body is ignored.
		if hook.maxBodySize > 0 {
			body := make([]byte, hook.maxBodySize)
			// Read full body, either until EOF or maxBodySize is reached
			for i := int64(0); i < hook.maxBodySize; {
				n, err := r.Body.Read(body)
				if err == io.EOF {
					break
				} else if err != nil && err != io.EOF {
					log.Printf("receive body failed: %s", err)
					w.Header().Add("Content-Type", "application/json")
					w.WriteHeader(500)
					fmt.Fprintf(w, "{\"status\":\"fail\",\"reason\":\"receive failure\"}")
					return
				} else {
					buffer.Write(body[:n])
					i += int64(n)
				}
			}
		}

		if hook.Background { // Execute command in background
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(200)
			fmt.Fprintf(w, "{\"status\":\"ok\"}")
			go func() {
				defer hook.Unlock()
				err := hook.Run(buffer.Bytes(), hook.Output)
				if err != nil {
					log.Printf("Hook \"%s\" failed: %s", hook.Name, err)
				} else {
					log.Printf("Hook \"%s\" completed", hook.Name)
				}
			}()
		} else {
			defer hook.Unlock()
			err := hook.Run(buffer.Bytes(), hook.Output)
			if err != nil {
				log.Printf("ERR: \"%s\" exec failure: %s", hook.Name, err)
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(500)
				fmt.Fprintf(w, "{\"status\":\"fail\",\"reason\":\"program error\"}")
			} else {
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(200)
				fmt.Fprintf(w, "{\"status\":\"ok\"}")
			}
		}
	}
}

func createHealthHandler() Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(200)
		fmt.Fprintf(w, "{\"status\":\"ok\"}")
	}
}

func createDefaultHandler() Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" || r.URL.Path == "/index.txt" {
			w.WriteHeader(200)
			fmt.Fprintf(w, "weblug - webhook receiver program\nhttps://codeberg.org/grisu48/weblug\n")
		} else if r.URL.Path == "/index.htm" || r.URL.Path == "/index.html" {
			w.WriteHeader(200)
			fmt.Fprintf(w, "<!DOCTYPE html><html><head><title>weblug</title></head>\n<body><p><a href=\"https://codeberg.org/grisu48/weblug\">weblug</a> - webhook receiver program</p>\n</body></html>")
		} else {
			respondNotFound(w, r)
		}
	}
}

func createRobotsHandler() Handler {
	return func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
		fmt.Fprintf(w, "User-agent: *\nDisallow: /")
	}
}

func respondNotFound(w http.ResponseWriter, r *http.Request) error {
	w.WriteHeader(404)
	_, err := fmt.Fprintf(w, "not found\n")
	return err
}
