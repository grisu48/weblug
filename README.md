# weblug

`weblug` is is a configurable webhook receiver that allows users to run arbitrary programs and script when a webhook is triggered.

The configuration happens via a [yaml file](weblug.yml). Read the [usage caveats](#caveats)!

`weblug` supports multiple webhooks via different URL paths, concurrency limitations, background execution and running webhooks as separate user (`uid`/`gid`).

## Usage

Webooks are defined via a yaml file. See [weblug.yml](weblug.yml) for an example configuration. Pass the yaml file(s) to `weblug` to starting the webserver and waiting for incoming webhooks:

    ./weblug YAML-FILE

`weblug` can run as any user, however for custom `uid`/`gid` webhooks, the program needs to run as root.

### Caveats

1. `weblug` should not face the open internet

weblug is expected to run behind a http reverse proxy (e.g. `apache` or `nginx`).
While the program itself does support tls, to avoid a whole class of security issues, `weblug` should never run on the open internet without a http reverse proxy.

2. `weblug` runs as root, when using custom UID/GIDs

In it's current implementation, `weblug` requires to remain running as root without dropping privileges when using custom UID/GIDs.

### TLS

`weblug` now supports tls. To generate self-signed keys

```
openssl genrsa -out weblug.key 2048
openssl req -new -x509 -sha256 -key weblug.key -out weblug1.pem -days 365
```

Then configure the `weblug.yml` file accordingly. You can use multiple keys/certificates.

## Build

    make               # Build weblug
    make static        # Make a static binary

Alternatively you there is also a [Taskfile](https://taskfile.dev)

    task
    task static        # Build static binary

## Run as systemd unit

This repository provides an example [weblug.service](weblug.service), which can be used to start `weblug` as systemd service.
This file can be placed in `/etc/systemd/system/weblug.service` and in conjunction with an adequate `weblug.yml` file e.g. in `/etc/weblug.yml` this provides a way of running weblug as a native systemd service.
