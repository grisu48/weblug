#!/bin/bash -ex
# Blackbox tests for weblug

cleanup() {
    # kill all processes whose parent is this process
    pkill -P $$
}
trap cleanup EXIT


if [[ $EUID != 0 && $UID != 0 ]]; then
	echo -e "(!!) WARNING: Cannot UID and GID webhook tests, because we're not running as root (!!)\n"
	echo -e "Continuing in 3 seconds\n\n\n"
	sleep 3
fi

# Secret environment variable, which must be removed by env sanitation.
export SECRET1="top5ecret"

rm -f testfile
../weblug test.yaml &
../weblug test_uid.yaml &
sleep 1

## Check touch webhook, which creates "testfile"

echo -e "\n\nChecking 'testfile' webhook ... "

curl --fail http://127.0.0.1:2088/webhooks/touch
if [[ ! -f testfile ]]; then
	echo "Testfile doesn't exist after running webhook touch"
	exit 1
fi
rm -f testfile

## Check background webhook, that sleeps for 5 seconds but returns immediately

echo -e "\n\nChecking 'background' webhook ... "

timeout 2 curl --fail http://127.0.0.1:2088/webhooks/background

## Check concurrency webhook, that allows only 2 requests at the same time (but sleeps for 5 seconds)

echo -e "\n\nChecking 'concurrency' webhook ... "

timeout 10 curl --fail http://127.0.0.1:2088/3 &
timeout 10 curl --fail http://127.0.0.1:2088/3 &
! timeout 2 curl --fail http://127.0.0.1:2088/3

## Check UID and GID webhooks, but only if we're root

echo -e "\n\nChecking 'uid/gid' webhook ... "

# Skip this test, if we're not root
if [[ $EUID == 0 || $UID == 0 ]]; then
	 curl --fail http://127.0.0.1:2088/webhooks/uid
	 curl --fail http://127.0.0.1:2088/webhooks/gid
else
	echo "Cannot UID and GID webhook tests, because we're not running as root"
fi

## Check environment variables

timeout 10 curl --fail 'http://127.0.0.1:2088/env'


## Check UID/GID handling

# Ensure weblug is running with UID=65534
pgrep -u 65534 weblug
# Ensure weblug2 (test_uid) is actually running
timeout 10 curl --fail 'http://127.0.0.1:2089/uid'

echo -e "\n\nall good"
