default: all
all: weblug

.PHONY: test

weblug: cmd/weblug/*.go
	go build -o weblug $^

static: cmd/weblug/*.go
	CGO_ENABLED=0 go build -ldflags="-w -s" -o weblug $^

test: weblug
	go test ./...
	sudo bash -c "cd test && ./blackbox.sh"
